# -*- coding: utf-8 -*-
"""
surveyName_SlackID_match_beta by @PaulineChane

NOTES
-some additional packages/checks included to account for user error, especially in survey dataframe


NEEDED IMPROVEMENTS:
-address final round of responses that needed manual search (excluded those that were Googled)
-improve search algorithm o(n) to accomodate growing user base
-add function/code to input manual match?
-concatenate survey/user.json dataframes
-pull user data directly from Slack using client

FUTURE SCRIPTS
-match prgm-introductions .json file user activity to user.json to identify discrepancies
-process "skills" and "resources" using API "skills" database (
    like LinkedIn and Indeed, or what will be rolled out for JOGL API) in survey response to put in column in concat. dataframe

FOLLOWING CODE modified and added to by @PaulineChane:

Created on Sun Mar 22 16:03:30 2020

@author: liubov

"""

import pandas as pd
import numpy as np
import os
import json

'''added following imports:'''
#fuzzywuzzy imported to account for user error in entering information
from fuzzywuzzy import process
from fuzzywuzzy import fuzz
import Levenshtein #improve fuzzywuzzy performance

def folder_to_df(folderpath):
    ''' function to get one dataframe from
    all json files in one folder '''

    df = pd.DataFrame({'type': []})  # empty dataframe

    for dirname, _, filenames in os.walk(folderpath):
        for filename in filenames:
            filepath = os.path.join(dirname, filename)
            print(os.path.join(dirname, filename))
            with open(filepath) as file:
                slack = json.load(file)
            other = pd.DataFrame(slack)

            df = pd.concat([other, df])  # df.set_index('text').join(other.set_index('text'))

    return df

'''added following defs:'''

def matchEmail_SurveyToSlack(str):
    '''first round of search to match email from survey to email in profile dict per Slack user node'''
    j=0
    for row in dfSlackUser.profile:
        currentProfileData = dfSlackUser.iloc[j, profileSlack_index]  # pull dict from cell
        if "email" in currentProfileData: #excludes bots and apps
            checkThisEmail = currentProfileData["email"]
            ratioMatch = fuzz.ratio(str.lower(),checkThisEmail.lower())
            if ratioMatch>=95:
                return j #returns row index of dfSlackUser containing best matched e-mail

        j+=1

    return -1
def matchFirstLast_SurveyToSlack(str):
    '''second round of search to match email from survey to email in profile dict per Slack user node'''
    j = 0
    for row in dfSlackUser.profile:
        currentProfileData = dfSlackUser.iloc[j, profileSlack_index]
        if ("real_name" in currentProfileData) & (" " in str):
            checkThisName = currentProfileData["real_name"]
            ratioMatch = fuzz.ratio(str.lower(), checkThisName.lower())
            if ratioMatch >= 95:
                return j  # returns row index of dfSlackUser containing best matched First/Last Name

        j += 1
    return -1 #return -1 if match not found
    #read all json files in one folder
#must contain user.json and survey.csv
folderpath = 'C:\\Users\\pcdol\\PycharmProjects\\JOGL_OpenCOVID10_Metastudy_SlackAnalysis\\OpenCOVID19_Initiative_Slack_export_Mar_2_2020_-_Mar_19_2020\\OpenCOVID19 Initiative Slack export Mar 2 2020 - Mar 19 2020\\users'  # "C:/Users/tupi

dfSlackUser = pd.DataFrame({'type': []})  # empty dataframe
'''added empty dataframe for survey responses'''
dfSurvey = pd.DataFrame({'type': []})

for dirname, _, filenames in os.walk(folderpath):
    for filename in filenames:
        filepath = os.path.join(dirname, filename)
        #print(os.path.join(dirname, filename))
        '''added check for reading .csv in folder'''
        if filename.endswith(".csv"):
            dfSurvey = pd.read_csv(filepath)
        else:
            with open(filepath) as file:
                slack = json.load(file)
            other = pd.DataFrame(slack)
            dfSlackUser = pd.concat([other, dfSlackUser]) # df.set_index('text').join(other.set_index('text'))


print('dataframe after concatenation', dfSlackUser.shape, dfSurvey.shape)

'''new additions to code here:'''
dfSurvey.insert(dfSurvey.columns.get_loc("name")+1,"Slack ID",0) #column to place Slack id

profileSlack_index = dfSlackUser.columns.get_loc("profile")
surveyEmail_index = dfSurvey.columns.get_loc("Email")
surveyName_index = dfSurvey.columns.get_loc("name")
surveyID_index = dfSurvey.columns.get_loc("Slack ID")
#run first match
#in onboardingSurvey_20200322.csv , total 157/233 or 68.2% responses matched
#when entries after last member joined as found in prgm-introductions/2020-03-19.json, total 157/199 or 78.9% responses matched
i = 0 #loop counter

for row in dfSurvey.Email:
    findThisEmail = dfSurvey.iloc[i,surveyEmail_index]
    rowIndex_dfSlackUser_emailMatch = matchEmail_SurveyToSlack(findThisEmail) #search for high match email
    if rowIndex_dfSlackUser_emailMatch >= 0: #if match found, add slack ID.
        dfSurvey.iloc[i, dfSurvey.columns.get_loc("Slack ID")] = dfSlackUser.iloc[
            rowIndex_dfSlackUser_emailMatch, dfSlackUser.columns.get_loc("id")]
    i+=1

#run second match for name in survey that include FIRST and LAST names (has whitespace)
#in onboardingSurvey_20200322.csv , total 172/199 or 73.8% responses matched
#when entries after last member joined as found in prgm-introductions/2020-03-19.json, total 170/199 or 85.4% responses matched
i = 0 #reset
for row in dfSurvey.name:
    if dfSurvey.iloc[i,surveyID_index] == 0: #skip those with matched IDs
        findFirstLast = dfSurvey.iloc[i,surveyName_index]
        rowIndex_dfSlackUser_FirstLastMatch = matchFirstLast_SurveyToSlack(findFirstLast)
        if rowIndex_dfSlackUser_FirstLastMatch >= 0:
            dfSurvey.iloc[i, dfSurvey.columns.get_loc("Slack ID")] = dfSlackUser.iloc[
                rowIndex_dfSlackUser_emailMatch, dfSlackUser.columns.get_loc("id")]
    i+=1
dfSurvey.to_csv("test.csv")#print to csv to check if it worked

#third round, 29 entries remaining, manual match
#of 29 entries remaining

'''from original code'''

# we want to join using the key columns, we need to set key to be the index in both df dataframes

# df.set_index('key').join(other.set_index('key'))
