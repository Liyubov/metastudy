---
title: "R Notebook"
output: html_notebook
---

```{r}
t <- read.delim('../1-extract-data/users_jogl.tsv')
```

```{r}
ids <- t$id
```

```{r}
dir.create('followers', showWarnings = F)
for (id in ids){
  system(paste0('curl --location --request GET https://jogl-backend.herokuapp.com/api/users/',id,'/followers > followers/',id,'.json'))
}
```


