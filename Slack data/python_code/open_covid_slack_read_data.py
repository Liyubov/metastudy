# -*- coding: utf-8 -*-
"""
Created on Sun Mar 22 16:03:30 2020

@author: liubov
"""


import pandas as pd
import numpy as np
import os
import json

def folder_to_df(folderpath):
    ''' function to get one dataframe from 
    all json files in one folder '''
    
    df = pd.DataFrame({'type' : []}) # empty dataframe

    for dirname, _, filenames in os.walk(folderpath):
        for filename in filenames:
            filepath = os.path.join(dirname, filename)
            print(os.path.join(dirname, filename))
            with open(filepath) as file:
                slack = json.load(file)
            other = pd.DataFrame(slack)
    
            df = pd.concat([other, df])#df.set_index('text').join(other.set_index('text'))
    
    return df




# read all json files in one folder
folderpath = 'C:/Users/lyubo/Documents/DATA_networks/covid19/OpenCOVID19_19_03/prgm-communications-team/' #"C:/Users/tupi


df = pd.DataFrame({'type' : []}) # empty dataframe

for dirname, _, filenames in os.walk(folderpath):
    for filename in filenames:
        filepath = os.path.join(dirname, filename)
        print(os.path.join(dirname, filename))
        with open(filepath) as file:
            slack = json.load(file)
        other = pd.DataFrame(slack)

        df = pd.concat([other, df])#df.set_index('text').join(other.set_index('text'))

        
print('dataframe after concatenation', df.shape)
df.head()
        
        
#we want to join using the key columns, we need to set key to be the index in both df dataframes

#df.set_index('key').join(other.set_index('key'))



